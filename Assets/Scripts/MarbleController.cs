﻿using UnityEngine;
using System.Collections;

public class MarbleController : MonoBehaviour {

	private int count;
	public GUIText countText;

	void Start()
	{
		count = 0;
		setCountText ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "PickUps") 
		{
			other.gameObject.SetActive (false);
			count = count + 1;
			setCountText();
		}
	}

	void setCountText()
	{
		countText.text = "Score: " + count.ToString();
	}

	public void restartGame()
	{
		Application.LoadLevel(0);
	}
}
